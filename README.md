# pyconza2020-ansible

Ansible setup for the BBB machines and related infrastructure for the PyCon ZA 2020 online conference

Setup the hostnames, domain and smtp server details as appropriate for your set.

Add the required ssh keys to roles/users/defaults to allow root access to the servers.

Replace the `domain` value with your domain, set an appropriate value of `ldap_domain` and `ldap_org`
and configure the smtp settings approriately.

Regenerate the `turn_key` value. The recommend command is `openssl rand -hex 16`.

When running this, the following password need to be specified as environment variables:
`LDAP_ADMIN_PASSWORD` - the ldap admin password
`GREENLIGHT_ADMIN_PASSWORD` - hardcoded password for the greenlight admin account (`gl_admin@<your domain>`)
`SMTP_PASSWORD` - password for the SMTP server

`bbb_main.pdf` is a single page blank pdf that replaces the default pdf presentation. Replace it
with something suitable for your use case (such as a list of sponsors) or set the `replace_pdf`
variable to false to keep the default BigBlueButton help text.
